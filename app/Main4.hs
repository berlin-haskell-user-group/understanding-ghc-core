module Main where

-- base
import Control.Monad

-- transformers
import Control.Monad.Trans.State

fibonacciStep :: State (Int, Int) ()
fibonacciStep = modify $ \(a, b) -> (a + b, a)

fibonacci :: Int -> Int
fibonacci n = fst $ execState (replicateM_ n fibonacciStep) (1, 1)

roundtrip :: Read a => Show a => a -> a
roundtrip = read . show

main = print $ fibonacci 45000
