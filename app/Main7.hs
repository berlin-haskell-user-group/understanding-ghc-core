module Main where

data FibonacciState = FibonacciState
  { current :: !Int
  , previous :: !Int
  }

fibonacciTailRecursive :: Int -> FibonacciState -> FibonacciState
fibonacciTailRecursive n FibonacciState{current, previous}
  | n >= 1 =
      fibonacciTailRecursive (n - 1) FibonacciState
        {current = current + previous, previous = current}
fibonacciTailRecursive _ result = result

fibonacci :: Int -> Int
fibonacci n = current $ fibonacciTailRecursive n FibonacciState
    {current = 1, previous = 1}

main = print $ fibonacci 45000000
