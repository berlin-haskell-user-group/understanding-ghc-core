module Main where

fibonacci :: Integer -> Integer
fibonacci n | n <= 2 = 1
fibonacci n = fibonacci (n - 1) + fibonacci (n - 2)

main = print $ fibonacci 45
