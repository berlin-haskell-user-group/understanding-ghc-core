module Main where

-- base
import Control.Monad

-- transformers
import Control.Monad.Trans.State.Strict

data FibonacciState = FibonacciState
  { current :: !Int
  , previous :: !Int
  }

fibonacciStep :: State FibonacciState ()
fibonacciStep = modify $ \FibonacciState{current, previous} -> FibonacciState{current = current + previous, previous = current}

fibonacci :: Int -> Int
fibonacci n = current $ execState (replicateM_ n fibonacciStep) FibonacciState{current = 1, previous = 1}

main = print $ fibonacci 45000000
