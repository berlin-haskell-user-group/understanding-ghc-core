module Main where

fibonacciTailRecursive :: Int -> (Int, Int) -> (Int, Int)
fibonacciTailRecursive n (current, previous) | n > 1 = fibonacciTailRecursive (n - 1) (current + previous, current)
fibonacciTailRecursive _ result = result

fibonacci :: Int -> Int
fibonacci n = fst $ fibonacciTailRecursive n (1, 1)

main = print $ fibonacci 45000000
